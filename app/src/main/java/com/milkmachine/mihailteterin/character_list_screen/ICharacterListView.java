package com.milkmachine.mihailteterin.character_list_screen;

import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;

import java.util.List;

public interface ICharacterListView {

    ICharacterListPresenter getPresenter();

    void showCharacterList(List<CharacterDb> characterList);

    String getCurrentHouseName();
}
