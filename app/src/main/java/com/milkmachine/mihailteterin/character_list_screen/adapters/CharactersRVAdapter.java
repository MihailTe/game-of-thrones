package com.milkmachine.mihailteterin.character_list_screen.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.milkmachine.mihailteterin.R;
import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;
import com.milkmachine.mihailteterin.utils.ConstantManager;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CharactersRVAdapter extends RecyclerView.Adapter<CharactersRVAdapter.ViewHolder> {

    private Context mContext;
    private List<CharacterDb> mCharacters;
    private final ViewHolder.CustomClickListener mClickListener;

    public CharactersRVAdapter(List<CharacterDb> characters, ViewHolder.CustomClickListener clickListener) {
        mCharacters = characters;
        mClickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mCharacters.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_character, parent, false);

        return new ViewHolder(convertView, mClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CharacterDb character = mCharacters.get(position);

        holder.setCharacter(character);

        holder.characterName.setText(character.getName());
        if (!character.getAliases().equals("")) {
            holder.characterInfo.setText(character.getAliases());
        } else holder.characterInfo.setText(character.getTitles());

        if (character.getHouse().equals(ConstantManager.STARKS)) {
            Picasso.with(mContext)
                    .load(R.drawable.stark_icon)
                    .into(holder.houseIcon);
        } else if (character.getHouse().equals(ConstantManager.LANNISTERS)) {
            Picasso.with(mContext)
                    .load(R.drawable.lanister_icon)
                    .into(holder.houseIcon);
        } else {
            Picasso.with(mContext)
                    .load(R.drawable.targaryens_icon)
                    .into(holder.houseIcon);
        }

    }

    public void loadItems(List<CharacterDb> characters) {
        mCharacters = characters;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView characterName, characterInfo;
        protected ImageView houseIcon;

        private CharacterDb mCharacter;
        private CustomClickListener mListener;

        public ViewHolder(View itemView, CustomClickListener listener) {
            super(itemView);

            mListener = listener;
            characterName = (TextView) itemView.findViewById(R.id.name_character);
            characterInfo = (TextView) itemView.findViewById(R.id.info_character);
            houseIcon = (ImageView) itemView.findViewById(R.id.icon_house);

            itemView.setOnClickListener(this);
        }

        public void setCharacter(CharacterDb character) {
            mCharacter = character;
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onCharacterClickListener(mCharacter.getId());
            }
        }

        public interface CustomClickListener {
            void onCharacterClickListener(Long id);
        }
    }

}
