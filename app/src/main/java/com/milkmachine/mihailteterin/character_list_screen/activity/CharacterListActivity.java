package com.milkmachine.mihailteterin.character_list_screen.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.milkmachine.mihailteterin.character_list_screen.fragments.CharacterListFragment;
import com.milkmachine.mihailteterin.R;
import com.milkmachine.mihailteterin.utils.ConstantManager;

import java.util.ArrayList;
import java.util.List;

public class CharacterListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private List<CharacterListFragment> mFragmentList = new ArrayList<>();

    //region==============================Lifecycle===============================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list_screen);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mFragmentList.add(CharacterListFragment.newInstance(ConstantManager.STARKS));
        mFragmentList.add(CharacterListFragment.newInstance(ConstantManager.LANNISTERS));
        mFragmentList.add(CharacterListFragment.newInstance(ConstantManager.TARGARYENS));

        setupToolbar();
        setupNavDrawer();
        setupViewPager();
    }

    //endregion

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void setupNavDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupViewPager() {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());

        for (CharacterListFragment fragment : mFragmentList) {
            adapter.addFragment(fragment);
        }

        mViewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_starks:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.nav_lannisters:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.nav_targaryens:
                mViewPager.setCurrentItem(2);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    class TabsAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();

        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }


        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getResources().getString(R.string.starks);
                case 1:
                    return getResources().getString(R.string.lannisters);
                case 2:
                    return getResources().getString(R.string.targaryens);
            }
            return "";
        }

        public void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }
}
