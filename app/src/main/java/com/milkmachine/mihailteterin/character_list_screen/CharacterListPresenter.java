package com.milkmachine.mihailteterin.character_list_screen;

import android.support.annotation.Nullable;

import com.milkmachine.mihailteterin.models.DataManager;
import com.milkmachine.mihailteterin.utils.AsyncTransformer;

import java.util.ArrayList;
import java.util.List;

public class CharacterListPresenter implements ICharacterListPresenter {
    private static CharacterListPresenter Instance = new CharacterListPresenter();
    private DataManager mDataManager = DataManager.getInstance();
    private List<ICharacterListView> mViews;

    private CharacterListPresenter() {
        mViews = new ArrayList<>();
    }

    public static CharacterListPresenter getInstance() {
        return Instance;
    }

    @Override
    public void dropView(ICharacterListView view) {
        mViews.remove(view);
    }

    @Override
    public void takeView(ICharacterListView characterListView) {
        mViews.add(characterListView);
    }

    @Override
    public void initView(ICharacterListView view) {

        if (view != null) {
            mDataManager.getCharactersByHouseFromDb(view.getCurrentHouseName())
                    .compose(new AsyncTransformer<>())
                    .subscribe(characters -> {
                                view.showCharacterList(characters);
                            },
                            error -> error.printStackTrace(),
                            () -> {
                            });
        }

    }


    @Nullable
    @Override
    public List<ICharacterListView> getViews() {
        return mViews;
    }
}
