package com.milkmachine.mihailteterin.character_list_screen;

import android.support.annotation.Nullable;

import java.util.List;

public interface ICharacterListPresenter {

    void takeView(ICharacterListView characterListView);

    void dropView(ICharacterListView view);

    void initView(ICharacterListView view);

    @Nullable
    List<ICharacterListView> getViews();
}
