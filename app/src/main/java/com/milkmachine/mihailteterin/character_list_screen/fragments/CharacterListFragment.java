package com.milkmachine.mihailteterin.character_list_screen.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.milkmachine.mihailteterin.character_list_screen.CharacterListPresenter;
import com.milkmachine.mihailteterin.character_list_screen.adapters.CharactersRVAdapter;
import com.milkmachine.mihailteterin.character_list_screen.ICharacterListPresenter;
import com.milkmachine.mihailteterin.character_list_screen.ICharacterListView;
import com.milkmachine.mihailteterin.character_list_screen.dividers.ItemDivider;
import com.milkmachine.mihailteterin.R;
import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;
import com.milkmachine.mihailteterin.character_screen.CharacterActivity;


import java.util.ArrayList;
import java.util.List;

public class CharacterListFragment extends Fragment implements ICharacterListView {

    private Context mContext;
    private CharacterListPresenter mPresenter = CharacterListPresenter.getInstance();
    private RecyclerView mRecyclerView;
    private List<CharacterDb> mCharacterList = new ArrayList<>();

    private CharactersRVAdapter mRecyclerViewAdapter;
    private String mCurrentHouseName;
    private static final String HOUSE_NAME = "house_name";

    private final CharactersRVAdapter.ViewHolder.CustomClickListener mItemClickListener =
            id -> {
                Intent intent = new Intent(mContext, CharacterActivity.class);
                intent.putExtra(CharacterActivity.CHARACTER_ID, id);
                startActivity(intent);
            };


    public static CharacterListFragment newInstance(String houseName) {
        CharacterListFragment fragment = new CharacterListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(HOUSE_NAME, houseName);
        fragment.setArguments(bundle);
        return fragment;
    }

    //region==================================Lifecycle============================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCurrentHouseName = getArguments().getString(HOUSE_NAME);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_character_list, container, false);

        mContext = container.getContext();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.character_list);
        mRecyclerViewAdapter = new CharactersRVAdapter(mCharacterList, mItemClickListener);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.addItemDecoration(new ItemDivider(mContext));
        mRecyclerView.setAdapter(mRecyclerViewAdapter);

        getPresenter().takeView(this);

        if (savedInstanceState == null)
            getPresenter().initView(this);

        return view;
    }

    @Override
    public void onDestroy() {
        getPresenter().dropView(this);
        super.onDestroy();
    }

    //endregion

    @Override
    public void showCharacterList(List<CharacterDb> characterList) {
        mCharacterList = characterList;
        mRecyclerViewAdapter.loadItems(mCharacterList);
    }

    @Override
    public ICharacterListPresenter getPresenter() {
        return mPresenter;
    }


    @Override
    public String getCurrentHouseName() {
        return mCurrentHouseName;
    }
}
