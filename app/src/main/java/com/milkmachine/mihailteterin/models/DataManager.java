package com.milkmachine.mihailteterin.models;

import android.content.Context;
import android.util.Log;

import com.milkmachine.mihailteterin.models.managers.RestService;
import com.milkmachine.mihailteterin.models.managers.ServiceGenerator;
import com.milkmachine.mihailteterin.models.network.response.CharacterRes;
import com.milkmachine.mihailteterin.models.network.response.HouseRes;
import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;
import com.milkmachine.mihailteterin.models.storage.models.CharacterDbDao;
import com.milkmachine.mihailteterin.models.storage.models.DaoSession;
import com.milkmachine.mihailteterin.utils.Converter;
import com.milkmachine.mihailteterin.utils.GoTApplication;
import com.milkmachine.mihailteterin.utils.NetworkStatusChecker;

import org.greenrobot.greendao.async.AsyncSession;
import org.greenrobot.greendao.rx.RxQuery;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

public class DataManager {

    private static DataManager Instance = null;

    private Context mContext;

    private DaoSession mDaoSession;
    private AsyncSession mAsyncDbSession;

    private RestService mRestService;

    private DataManager() {
        mContext = GoTApplication.getContext();
        mDaoSession = GoTApplication.getDaoSession();
        mRestService = ServiceGenerator.createService(RestService.class);
        mAsyncDbSession = mDaoSession.startAsyncSession();
    }

    public static DataManager getInstance() {
        if (Instance == null) {
            Instance = new DataManager();
        }
        return Instance;
    }

    public boolean networkAvailable() {
        return NetworkStatusChecker.isNetworkAvailable(mContext);
    }

    //region ============ Network ==============

    public Observable<HouseRes> getHousesFromNetwork() {
        return Observable.concat(
                mRestService.getStarkHouse(),
                mRestService.getLannisterHouse(),
                mRestService.getTargaryenHouse()
        );
    }

    public Observable<CharacterRes> getCharacterFromNetwork(String url) {
        return mRestService.getCharacter(url);
    }

    //endregion

    //region ============ Database =============

    public boolean isReadyDatabase() {
        return mDaoSession.getCharacterDbDao().loadAll().size() > 0;
    }


    public Observable<List<CharacterDb>> getCharactersByHouseFromDb(String house) {

        return mDaoSession.getCharacterDbDao().queryBuilder()
                .where(CharacterDbDao.Properties.House.eq(house))
                .orderAsc(CharacterDbDao.Properties.Name)
                .rx()
                .list();
    }

    public Observable<CharacterDb> getCharacterFromDb(Long id) {

        return mDaoSession.getCharacterDbDao().queryBuilder()
                .where(CharacterDbDao.Properties.Id.eq(id))
                .rx()
                .unique();
    }

    public void prepareDatabase() {

        List<CharacterDb> charactersDbList = new ArrayList<>();

        getHousesFromNetwork()
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.io())
                .flatMap(houseRes -> {
                    List<String> urls = houseRes.getSwornMembers();
                    String houseName = houseRes.getName();
                    String houseWords = houseRes.getWords();

                    return Observable.from(urls)
                            .flatMap(url -> getCharacterFromNetwork(url)
                                    .map(characterRes -> {
                                        CharacterDb characterDb = new CharacterDb();

                                        Long characterId = Long.parseLong(Converter.getLastSegmentUrl(characterRes.getUrl()));
                                        characterDb.setId(characterId);
                                        characterDb.setName(characterRes.getName());
                                        characterDb.setBornDate(characterRes.getBorn());
                                        characterDb.setAliases(Converter.getStringFromList(characterRes.getAliases()));
                                        characterDb.setTitles(Converter.getStringFromList(characterRes.getTitles()));
                                        characterDb.setDieDate(characterRes.getDied());
                                        characterDb.setHouse(houseName);
                                        characterDb.setWords(houseWords);

                                        if (!characterRes.getFather().isEmpty()) {
                                            getCharacterFromNetwork(characterRes.getFather())
                                                    .subscribe(fatherRes -> {
                                                        Long fatherId = Long.parseLong(Converter.getLastSegmentUrl(fatherRes.getUrl()));
                                                        characterDb.setFatherId(fatherId);
                                                        characterDb.setFatherName(fatherRes.getName());
                                                    });

                                        }

                                        if (!characterRes.getMother().isEmpty()) {
                                            getCharacterFromNetwork(characterRes.getMother())
                                                    .subscribe(motherRes -> {
                                                        Long motherId = Long.parseLong(Converter.getLastSegmentUrl(motherRes.getUrl()));
                                                        characterDb.setMotherId(motherId);
                                                        characterDb.setMotherName(motherRes.getName());
                                                    });

                                        }


                                        List<String> tvSeries = characterRes.getTvSeries();

                                        if (!tvSeries.isEmpty())
                                            characterDb.setLastSeason(tvSeries.get(tvSeries.size() - 1));

                                        return characterDb;
                                    }));

                }).subscribe(
                characterDb -> charactersDbList.add(characterDb),
                error -> error.printStackTrace(),
                () -> mAsyncDbSession.insertOrReplaceInTx(CharacterDb.class, charactersDbList)
        );
    }

    public AsyncSession getAsyncDbSession() {
        return mAsyncDbSession;
    }

    //endregion

}
