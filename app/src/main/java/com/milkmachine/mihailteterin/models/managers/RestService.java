package com.milkmachine.mihailteterin.models.managers;

import com.milkmachine.mihailteterin.models.network.response.CharacterRes;
import com.milkmachine.mihailteterin.models.network.response.HouseRes;

import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface RestService {

    @GET("houses/362")
    Observable<HouseRes> getStarkHouse();

    @GET("houses/378")
    Observable<HouseRes> getTargaryenHouse();

    @GET("houses/229")
    Observable<HouseRes> getLannisterHouse();

    @GET()
    Observable<CharacterRes> getCharacter(@Url String url);
}
