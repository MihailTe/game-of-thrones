package com.milkmachine.mihailteterin.utils;

import java.util.List;

public class Converter {

    public static String getStringFromList(List<String> list) {

        String separator = ", ";

        if (list.size() == 0) {
            return "";
        } else if (list.size() == 1) {
            return list.get(0);
        } else {
            StringBuilder builder = new StringBuilder();

            for (String s : list) {
                builder.append(s + separator);
            }

            builder.delete(builder.lastIndexOf(separator), builder.length() - 1);
            return builder.toString();
        }
    }

    public static String getLastSegmentUrl(String url) {
        String[] segments = url.split("/");

        return segments[segments.length - 1];
    }
}
