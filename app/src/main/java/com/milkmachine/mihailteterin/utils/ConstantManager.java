package com.milkmachine.mihailteterin.utils;

public interface ConstantManager {

    String STARKS = "House Stark of Winterfell";
    String LANNISTERS = "House Lannister of Casterly Rock";
    String TARGARYENS = "House Targaryen of King's Landing";
}
