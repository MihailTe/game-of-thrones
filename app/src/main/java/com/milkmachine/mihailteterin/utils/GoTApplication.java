package com.milkmachine.mihailteterin.utils;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;
import com.milkmachine.mihailteterin.models.storage.models.DaoMaster;
import com.milkmachine.mihailteterin.models.storage.models.DaoSession;

import org.greenrobot.greendao.database.Database;

public class GoTApplication extends Application {

    private static Context sContext;
    private static DaoSession sDaoSession;


    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "gameOfThrones-db");
        Database database = helper.getWritableDb();
        sDaoSession = new DaoMaster(database).newSession();

        Stetho.initializeWithDefaults(this);
    }

    public static Context getContext() {
        return sContext;
    }

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }
}
