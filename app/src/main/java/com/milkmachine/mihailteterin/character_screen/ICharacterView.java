package com.milkmachine.mihailteterin.character_screen;

import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;

public interface ICharacterView {

    ICharacterPresenter getPresenter();

    void showCharacterInfo(CharacterDb character);

    Long getCharacterId();

}
