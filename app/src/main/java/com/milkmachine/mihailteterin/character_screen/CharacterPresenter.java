package com.milkmachine.mihailteterin.character_screen;

import android.support.annotation.Nullable;

import com.milkmachine.mihailteterin.models.DataManager;
import com.milkmachine.mihailteterin.utils.AsyncTransformer;

import rx.Subscription;

public class CharacterPresenter implements ICharacterPresenter {
    private static CharacterPresenter Instance = new CharacterPresenter();

    private DataManager mDataManager = DataManager.getInstance();
    private ICharacterView mCharacterView;
    private Subscription mSubscription;

    private CharacterPresenter() {
    }

    public static CharacterPresenter getInstance() {
        return Instance;
    }

    @Override
    public void dropView() {
        mCharacterView = null;
        mSubscription.unsubscribe();
    }

    @Override
    public void takeView(ICharacterView characterView) {
        mCharacterView = characterView;
    }

    @Override
    public void initView() {
        if (getView() != null) {
                mSubscription = mDataManager.getCharacterFromDb(getView().getCharacterId())
                        .compose(new AsyncTransformer<>())
                        .cache()
                        .subscribe(character -> getView().showCharacterInfo(character),
                                error -> error.printStackTrace());
        }
    }

    @Nullable
    @Override
    public ICharacterView getView() {
        return mCharacterView;
    }
}
