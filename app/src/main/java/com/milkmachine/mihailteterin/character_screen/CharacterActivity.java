package com.milkmachine.mihailteterin.character_screen;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.milkmachine.mihailteterin.R;
import com.milkmachine.mihailteterin.models.storage.models.CharacterDb;
import com.milkmachine.mihailteterin.utils.ConstantManager;
import com.milkmachine.mihailteterin.databinding.ActivityCharacterScreenBinding;
import com.squareup.picasso.Picasso;

public class CharacterActivity extends AppCompatActivity implements ICharacterView {

    public static final String CHARACTER_ID = "character id";
    private CharacterPresenter mPresenter = CharacterPresenter.getInstance();
    private Long mCharacterId;
    private CharacterDb mCharacter;

    private ActivityCharacterScreenBinding mBinding;

    //region====================================Lifecycle==========================================

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_character_screen);
        mBinding.contentActivityCharacter.setActivity(this);

        mCharacterId = getIntent().getLongExtra(CHARACTER_ID, 0);

        mPresenter.takeView(this);
        mPresenter.initView();

        setupToolbar();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    //endregion


    @Override
    public ICharacterPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void showCharacterInfo(CharacterDb character) {
        if (character!=null){
            mCharacter = character;
            setupAppbarImage(mCharacter);
            mBinding.setCharacter(mCharacter);

            if (!mCharacter.getDieDate().isEmpty()) {
                showSnackbarAboutDeath(mCharacter.getLastSeason());
            }
        } else {
            onBackPressed();
        }

    }

    @Override
    public Long getCharacterId() {
        return mCharacterId;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    private void setupAppbarImage(CharacterDb character) {


        ImageView houseImage = mBinding.imageHouse;

        switch (character.getHouse()) {
            case ConstantManager.STARKS:
                Picasso.with(this)
                        .load(R.drawable.stark)
                        .into(houseImage);
                break;
            case ConstantManager.LANNISTERS:
                Picasso.with(this)
                        .load(R.drawable.lannister)
                        .into(houseImage);
                break;
            case ConstantManager.TARGARYENS:
                Picasso.with(this)
                        .load(R.drawable.targarien)
                        .into(houseImage);
                break;
        }

    }

    private void showSnackbarAboutDeath(String season) {
        String message;

        if (season == null) {
            message = "Character died";
        } else {
            message = "Character died in the ".concat(season);
        }

        Snackbar.make(mBinding.layoutCharacterContainer, message, Snackbar.LENGTH_LONG).show();
    }

    public void goToCharacterScreen(Long id) {
        Intent intent = new Intent(this, CharacterActivity.class);
        intent.putExtra(CHARACTER_ID, id);
        startActivity(intent);
    }

}
