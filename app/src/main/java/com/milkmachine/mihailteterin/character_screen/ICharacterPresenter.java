package com.milkmachine.mihailteterin.character_screen;

import android.support.annotation.Nullable;

public interface ICharacterPresenter {

    void takeView(ICharacterView characterView);

    void dropView();

    void initView();

    @Nullable
    ICharacterView getView();
}
