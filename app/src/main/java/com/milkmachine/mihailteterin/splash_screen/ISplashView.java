package com.milkmachine.mihailteterin.splash_screen;

public interface ISplashView {

    void showLoad();

    void hideLoad();

    void showNetworkError();

    ISplashPresenter getPresenter();

    void goToNextScreen();

    void goToNextScreenWithDelay();
}
