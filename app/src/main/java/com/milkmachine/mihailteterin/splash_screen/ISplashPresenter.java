package com.milkmachine.mihailteterin.splash_screen;

import android.support.annotation.Nullable;

public interface ISplashPresenter {

    void takeView(ISplashView splashView);

    void dropView();

    void initView();

    void prepareData();

    @Nullable
    ISplashView getView();
}
