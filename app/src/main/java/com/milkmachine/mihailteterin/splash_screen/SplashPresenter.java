package com.milkmachine.mihailteterin.splash_screen;

import android.support.annotation.Nullable;

import com.milkmachine.mihailteterin.models.DataManager;

import org.greenrobot.greendao.async.AsyncOperationListener;
import org.greenrobot.greendao.async.AsyncSession;

public class SplashPresenter implements ISplashPresenter {
    private static SplashPresenter Instance = new SplashPresenter();
    private DataManager mDataManager = DataManager.getInstance();
    private AsyncSession mAsyncSession;
    private ISplashView mSplashView;

    private final AsyncOperationListener mDbListener = operation -> {
        if (operation.isCompletedSucessfully()) {
            if (getView() != null) {
                getView().goToNextScreen();
            }
        }
    };

    private SplashPresenter() {
    }

    @Override
    public void prepareData() {
        if (mDataManager.networkAvailable()) {
            mAsyncSession = mDataManager.getAsyncDbSession();
            mAsyncSession.setListenerMainThread(mDbListener);
            mDataManager.prepareDatabase();
        } else {
            if (getView()!=null) {
                getView().showNetworkError();
            }
        }
    }

    public static SplashPresenter getInstance() {
        return Instance;
    }

    @Override
    public void dropView() {
        mSplashView = null;
    }

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showLoad();
            if (mDataManager.isReadyDatabase()) {
                getView().goToNextScreenWithDelay();
            } else {
                prepareData();
            }
        }
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }
}
