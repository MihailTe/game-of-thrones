package com.milkmachine.mihailteterin.splash_screen;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.milkmachine.mihailteterin.character_list_screen.activity.CharacterListActivity;
import com.milkmachine.mihailteterin.R;


public class SplashScreenActivity extends AppCompatActivity implements ISplashView {
    SplashPresenter mPresenter = SplashPresenter.getInstance();

    private CoordinatorLayout mCoordinatorLayout;
    private ProgressBar mProgressBar;
    private final View.OnClickListener mClickListener = view -> mPresenter.prepareData();

    //====================================Lifecycle================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_splash);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        hideSystemAppbar();

        mPresenter.takeView(this);
        mPresenter.initView();

    }

    @Override
    protected void onDestroy() {
        getPresenter().dropView();
        super.onDestroy();
    }

    //endregion

    public void hideSystemAppbar() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void showLoad() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showNetworkError() {
        Snackbar.make(mCoordinatorLayout, getResources().getString(R.string.network_unavailable), Snackbar.LENGTH_INDEFINITE)
                .setAction(getResources().getString(R.string.retry), mClickListener)
                .show();
    }

    @Override
    public ISplashPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public void goToNextScreen() {
        Intent intent = new Intent(this, CharacterListActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToNextScreenWithDelay() {
        Handler handler = new Handler();
        handler.postDelayed(this::goToNextScreen, 2500);
    }
}
